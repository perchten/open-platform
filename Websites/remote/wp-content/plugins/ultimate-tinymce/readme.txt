=== Ultimate TinyMCE ===
Contributors: Josh Lobe 
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=A9E5VNRBMVBCS
Tags: tinymce, editor, buttons, button, add, cut, copy, paste, font, font style, font select, font color, table, tables, visual editor, emoticons, emotions, smilies, smiley, smily, search, replace, colors, color, anchor, advance, advanced, links, link, popup, javascript, upgrade, update, admin, image, images, citations, preview, html, custom, custom css, css, borders, pages, posts, pretty, colorful
Requires at least: 3.2.1
Tested up to: 3.3
Stable tag: 1.3

Description: Beef up your visual tinymce editor with a plethora of advanced options: Emoticons, Tables, Styles, Advanced links, images, and drop-downs, too many features to list.

== Description ==
Are you a visual person?  Do the four letters "HTML" send you running for the hills; but you still want to create beautiful blogs like the pros?  Then this is the plugin for you!

<strong>Ultimate TinyMCE</strong> will expand the default array of buttons in the visual tinymce editor, giving you the power to visually create your pages and posts. No need for mucking about in HTML and CSS.

<strong>Here are just a few features which make this plugin so powerful:</strong>
<ul><li>Add, create, and manipulate as many tables as you like.</li><li>Add emoticons to express your mood as you are writing your posts/pages.</li><li>Now you can use subscript and superscript text.</li><li>Added advanced features for images (such as mouseover and mouseout).</li><li>Added advanced features for ordered and unordered drop-down lists.</li><li>Now features a search and replace tool.</li><li>Easily add page anchors to posts that become too long to scroll through all the content.</li><li>Added new feature to the "link" button now enabling you to open links in your own javascript popup windows, without writing a single line of code.  You can even control the size and position of your popup window.</li><li><strong>And much more... too many features to list.</strong></li><li>This plugin is in very active development, and new versions will be released adding even more customization and functionality.</li></ul>

If you like this plugin, <strong>Please Leave A Rating</strong>.  Also, click "works" if you are indeed satisfied with the plugin.  Thank you.

Also, <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=A9E5VNRBMVBCS">the smallest donations will be gratefully accepted if you wish to click here</a>.  <strong>Donations help to continue and support future upgrades and releases.</strong>  Please consider donating if you are extremely pleased with this plugin and will continue using it on your site; especially if you are operating a commercial website.  Thank you!

<strong>Additional Plugin Resources:</strong>

<a href="http://www.youtube.com/watch?v=7thuFbZWniE">Screencast 1</a> - You can view my screencast showing the plugin in action; and detailing some of the functionality.  <strong>NOTE:</wtrong>  This screencast shows the google fonts.  The google fonts are NOT included in this plugin (see changelog for more info).

You can use my webpage <a href="http://www.joshlobe.com/2011/11/adding-buttons-to-tinymce-in-wordpress/">Here</a> for expedited help and support with this plugin; or to suggest additional features for future updates.


== Installation ==

1. Upload the plugin to your 'wp-content/plugins' directory, or download and install automatically through your admin panel.

     
2. Activate the plugin through the 'Plugins' menu in WordPress.

3. There is not an options menu; this is as simple as it gets.  The buttons are added automatically.  Just open any post or page in the visual editor to see your new buttons.

4. If you are updating; no problem.  Just click the automatic update and everything will be done for you.  Since there are no settings to be saved, nothing gets lost.  After updating, the next time you open your editor you will immediately have the new functionality.


== Frequently Asked Questions ==

= The color picker feature always shows hex code #000000 regardless of what color I click. =

This is a bug in the TinyMCE core software.  This bug, as far as I know, is only seen when using Mozilla Firefox version 7.  Until the good folks over at TinyMCE can provide an upgrade, the only solution right now is to use another browser.

= How do I use the Anchors button =

<ol><li>Highlight the text, image, or other content where you would like to insert the anchor.</li><li>Click the anchor button and enter a unique name for the anchor.</li><li>Now, highlight the text where you want the user to click TO BE SENT to the anchor you created.<li><li>Click the Link button in the toolbar, and on the popup window click anchors and choose the name of your unique anchor you created.</li></ol>

= I still don't see my buttons after activating the plugin =

You may have to click the button titled "enable kitchen sink" from your top row.  This will expand the editor buttons.

== Screenshots ==

1. <strong>Shows the two button rows added to the visual editor.</strong>
2. <strong>The visual edit style CSS box.  Easily edit your CSS styles from a visual mode, without any need to know CSS or HTML.</strong>
3. <strong>The attributes box showing the many different possible events you may add.</strong>
4. <strong>Easily create gorgeous tables using only the visual editor.</strong>
5. <strong>The advanced link box.  Here, you can have your links display as javascript popup windows; among other features.</strong>

== Features ==

* Now includes an admin panel to choose which buttons you would like to show in your editor.
* Complete control over custom CSS styles using drop-down lists.
* Advanced link and image inserts, including mouseover and mouseout, and even open links in javascript windows... all without writing a single line of code.
* Adds buttons for cut, copy, and paste.
* Adds a button to insert horizontal rows.
* A dropdown list for font-styles.
* A dropdown list for font-size.
* Adds colorpickers for font foreground and background colors.
* Adds anchors, making their usage extremely easy.  (Ever wondered how you click a link and it auto-scrolls you halfway down the page??  Here's how.)
* Includes a grand total of 48 emoticons... and counting.  See them in both your admin panel AND your pages/posts.
* Adds a new row for adding tables and manipulating their properties individually.  Add as many tables as you like, and change the properties of each, without affecting your other tables.

*Simply too many features to list!!

== Changelog ==

= 1.3 =
* Replaced "Example Text" with small icons of each button.  Now you can visually see which button you are enabling or disabling.
* Added a stylesheet for future upgrades.  This fixed a small bug where it was throwing an error.  I apologize if anyone was caught by this "speedbump".

= 1.2 =
* Added an admin control where you can choose which buttons you would like to display in the editor.
* Adds an option menu in the admin panel under Settings -> Ultimate TinyMCE.
* Simply select which buttons you would like to add to your editor.
* Note: All buttons are selected by default.

= 1.1 =
* This plugin is exactly like my Ultimate Visual Editor Upgrade plugin; except this plugin does NOT include the 291 Google Webfonts.  I realized the increased load the fonts puts on the server because each font calls a separate HTML request.  So, I created this plugin to use in the mean time until I can figure out a better solution.
* Plugin was created.