=== Plugin Name ===
Contributors: roidayan
Donate link: http://roidayan.com
Tags: events, facebook
Requires at least: 3.2.1
Tested up to: 3.2.1
Stable tag: 1.0.1

A widget for displaying facebook events of a fan page.

== Description ==

A widget for displaying facebook events of a fan page.
Based on code by Mike Dalisay
http://www.codeofaninja.com/2011/07/display-facebook-events-to-your-website.html

== Installation ==

Extract the folder into your wordpress plugins directory.
You'll have a new widget in the widgets page, after activating the widget
you need to edit the settings.

== Frequently Asked Questions ==

= How to make all events visible wihout scrolling? =

In container height fill the value 'auto'.

= How to use the background color of the theme? =

In background color fill the value 'inherit'.

== Screenshots ==

1. example

== Changelog ==

= 1.0.1 =
* fixed bug in echo statement.

= 1.0 =
* first