#@ amr_ical_list_lang
msgid ""
msgstr ""
"Project-Id-Version: amr_ical_list_lang\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-09-01 23:54+1000\n"
"PO-Revision-Date: \n"
"Last-Translator: Anna-marie <Redpath>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: \n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: C:/web/wpbeta/wp/wp-content/plugins/amr-ical-events-list\n"
"X-Textdomain-Support: yes"

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:192
#@ amr_ical_list_lang
msgid "Year"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:193
#@ amr_ical_list_lang
msgid "Quarter"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:194
#@ amr_ical_list_lang
msgid "Astronomical Season"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:195
#@ amr_ical_list_lang
msgid "Traditional Season"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:196
#@ amr_ical_list_lang
msgid "Western Zodiac"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:197
#@ amr_ical_list_lang
msgid "Month"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:198
#@ amr_ical_list_lang
msgid "Week"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:199
#@ amr_ical_list_lang
msgid "Day"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:213
#@ amr_ical_list_lang
msgid "When"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:214
#@ amr_ical_list_lang
msgid "What"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:215
#@ amr_ical_list_lang
msgid "Where"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:309
#@ amr_ical_list_lang
msgid "Show in Google map"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:315
#@ amr_ical_list_lang
msgid "Show location in Google Maps"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:332
#@ amr_ical_list_lang
msgid "Venue"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:333
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:379
#@ amr_ical_list_lang
msgid "Description"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:348
#@ amr_ical_list_lang
msgid "Date"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:349
#@ amr_ical_list_lang
msgid "Class"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:350
#@ amr_ical_list_lang
msgid "Room"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:380
#@ amr_ical_list_lang
msgid "Timing"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:693
#@ amr_ical_list_lang
msgid "Events plugin by anmari"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:713
#@ amr_ical_list_lang
msgid "No events found within start and end date"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:726
#@ amr_ical_list_lang
msgid "Resetting options..."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:727
#@ amr_ical_list_lang
msgid "Options Deleted..."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:728
#@ amr_ical_list_lang
msgid "Error deleting option..."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-config.php:738
#@ amr_ical_list_lang
msgid " Converting option key to lowercase"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:7
#@ amr_ical_list_lang
msgid "Minimum Php version "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:11
#@ amr_ical_list_lang
msgid "The <a href=\"http://au.php.net/manual/en/class.datetime.php\"> DateTime Class </a> must be enabled on your system for this plugin to work. They may need to be enabled at compile time.  The class should exist by default in PHP version 5.2."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:24
#@ amr_ical_list_lang
msgid "Daily"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:25
#@ amr_ical_list_lang
msgid "Weekly"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:26
#@ amr_ical_list_lang
msgid "Monthly"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:27
#@ amr_ical_list_lang
msgid "Yearly"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:28
#@ amr_ical_list_lang
msgid "Hourly"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:29
#@ amr_ical_list_lang
msgid "on certain dates"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:30
#@ amr_ical_list_lang
msgid "day"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:31
#@ amr_ical_list_lang
msgid "week"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:32
#@ amr_ical_list_lang
msgid "month"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:33
#@ amr_ical_list_lang
msgid "year"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:34
#@ amr_ical_list_lang
msgid "hour"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:38
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:45
#@ amr_ical_list_lang
msgid "th"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:41
#@ amr_ical_list_lang
msgid "st"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:42
#@ amr_ical_list_lang
msgid "nd"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:43
#@ amr_ical_list_lang
msgid "rd"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:51
#@ amr_ical_list_lang
msgid "the first"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:52
#@ amr_ical_list_lang
msgid "every"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:53
#@ amr_ical_list_lang
msgid "the last"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:54
#, php-format
#@ amr_ical_list_lang
msgid "the %s last"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:55
#, php-format
#@ amr_ical_list_lang
msgid "the %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:103
#, php-format
#@ amr_ical_list_lang
msgid "Weeks start on %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:132
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:139
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:155
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:233
#@ amr_ical_list_lang
msgid " and "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:141
#@ amr_ical_list_lang
msgid "every "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:170
#@ amr_ical_list_lang
msgid " or "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:204
#, php-format
#@ amr_ical_list_lang
msgid "Every %s %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:205
#, php-format
#@ amr_ical_list_lang
msgid "every %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:210
#, php-format
#@ amr_ical_list_lang
msgid "On %s instance within %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:213
#, php-format
#@ amr_ical_list_lang
msgid "%s times"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:216
#, php-format
#@ amr_ical_list_lang
msgid "until %s %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:219
#, php-format
#@ amr_ical_list_lang
msgid " if month is %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:221
#, php-format
#@ amr_ical_list_lang
msgid " in %s weeks of the year"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:223
#, php-format
#@ amr_ical_list_lang
msgid "on %s day of the year"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:224
#, php-format
#@ amr_ical_list_lang
msgid "on %s day of each month"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:226
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:228
#, php-format
#@ amr_ical_list_lang
msgid "on %s "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:232
#, php-format
#@ amr_ical_list_lang
msgid " of the %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:238
#, php-format
#@ amr_ical_list_lang
msgid "at the %s hour"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:239
#, php-format
#@ amr_ical_list_lang
msgid "at the %s minute"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:240
#, php-format
#@ amr_ical_list_lang
msgid "at the %s second"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:338
#@ amr_ical_list_lang
msgid "Add to google calendar"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:351
#@ amr_ical_list_lang
msgid "Add event to google"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:433
#@ amr_ical_list_lang
msgid "Last Refresh time unexpectedly not available"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:437
#@ amr_ical_list_lang
msgid "Refresh calendars"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:438
#, php-format
#@ amr_ical_list_lang
msgid "Last refresh was at %s. "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:440
#@ amr_ical_list_lang
msgid "Remote file had no modifications. "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:444
#, php-format
#@ amr_ical_list_lang
msgid "The remote file was last modified on %s."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:449
#@ amr_ical_list_lang
msgid "Click to refresh"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:456
#@ amr_ical_list_lang
msgid "Total events: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:465
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:475
#, php-format
#@ amr_ical_list_lang
msgid "Subscribe to %s Calendar"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:482
#@ amr_ical_list_lang
msgid "Calendar"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:748
#@ amr_ical_list_lang
msgid "Change Timezone"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:749
#, php-format
#@ amr_ical_list_lang
msgid "Timezone: %s, Click for %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:771
#@ amr_ical_list_lang
msgid "Sent by "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:912
#@ amr_ical_list_lang
msgid "Event Link"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1317
#, php-format
#@ amr_ical_list_lang
msgid "Week  %u"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1790
#, php-format
#@ amr_ical_list_lang
msgid "Unable to load or cache ical calendar %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1835
#@ amr_ical_list_lang
msgid "This feature requires the plugin amr-events"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1836
#@ amr_ical_list_lang
msgid "Get it here"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1868
#@ amr_ical_list_lang
msgid "No url entered - did you want events from posts ?"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:1891
#@ amr_ical_list_lang
msgid "No ical components requested for display"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2026
#, php-format
#@ amr_ical_list_lang
msgid "Invalid Ical URL %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2041
#, php-format
#@ amr_ical_list_lang
msgid "Invalid Ical URL passed in query string %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2125
#@ amr_ical_list_lang
msgid "AmR iCal Events List"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2126
#@ amr_ical_list_lang
msgid "iCal Events List"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2164
#@ amr_ical_list_lang
msgid "Uncaught exception: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list-main.php:2165
#@ amr_ical_list_lang
msgid "<br /><br />An error in the input data may prevent correct display of this page.  Please advise the administrator as soon as possible."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list.php:41
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-events-list.php:43
#@ amr_ical_list_lang
msgid "Settings"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:46
#@ amr_ical_list_lang
msgid "Invalid Number of Lists"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:143
#@ amr_ical_list_lang
msgid "Error in form - calprop array not found"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:196
#@ amr_ical_list_lang
msgid "General:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:202
#@ amr_ical_list_lang
msgid "Name"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:205
#@ amr_ical_list_lang
msgid "Internal Description"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:208
#@ amr_ical_list_lang
msgid "List HTML Style"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:210
#@ amr_ical_list_lang
msgid "Table"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:211
#@ amr_ical_list_lang
msgid "Lists for rows"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:212
#@ amr_ical_list_lang
msgid "Breaks for rows!"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:213
#@ amr_ical_list_lang
msgid "Table with lists in cells (original)"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:215
#@ amr_ical_list_lang
msgid "Default Event URL"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:226
#@ amr_ical_list_lang
msgid "Define maximums:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:244
#@ amr_ical_list_lang
msgid "Select components to show:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:265
#@ amr_ical_list_lang
msgid "Define grouping:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:281
#@ amr_ical_list_lang
msgid "Calendar properties"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:303
#@ amr_ical_list_lang
msgid "Specify component contents:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:332
#@ amr_ical_list_lang
msgid "Column Headings:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:354
#@ amr_ical_list_lang
msgid "Significant effort goes into these plugins to ensure that they <strong>work straightaway</strong> with minimal effort, are easy to use but <strong>very configurable</strong>, that they are <strong>well tested</strong> and that they produce <strong>valid html and css</strong> both at the front and admin area."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:355
#@ amr_ical_list_lang
msgid "If you have a feature request, please do let me know. "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:357
#@ amr_ical_list_lang
msgid "Plugin support"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:359
#@ amr_ical_list_lang
msgid "Rate it at WP"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:362
#@ amr_ical_list_lang
msgid "Donate"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:363
#@ amr_ical_list_lang
msgid "Plugin feed"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:365
#@ amr_ical_list_lang
msgid "Comments feed"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:369
#, php-format
#@ amr_ical_list_lang
msgid "Now you can <b>create events</b> and <b>ics feeds</b> directly in wordpress - See screenshots and demo at %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:374
#, php-format
#@ amr_ical_list_lang
msgid "Thank you for using %s!"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:397
#@ amr_ical_list_lang
msgid "Unable to create Custom css file for you to edit if you wish - not essential."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:401
#, php-format
#@ amr_ical_list_lang
msgid "Copied %s1 to %s2 to allow custom css"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:412
#@ amr_ical_list_lang
msgid "Talk to your webhost if the current time and/or daylight saving change below is incorrect:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:414
#@ amr_ical_list_lang
msgid "Timezone: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:416
#@ amr_ical_list_lang
msgid "Current UTC offset: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:428
#, php-format
#@ amr_ical_list_lang
msgid "Switches to %s on %s. GMT offset: %d (%s)"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:434
#@ amr_ical_list_lang
msgid "Current time (unlocalised): "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:451
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:558
#@ amr_ical_list_lang
msgid "General Options"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:453
#@ amr_ical_list_lang
msgid "Number of Ical Lists:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:457
#@ amr_ical_list_lang
msgid "Message if no events found: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:467
#@ amr_ical_list_lang
msgid "Do not give credit to the author"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:471
#@ amr_ical_list_lang
msgid " Do not generate css"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:473
#@ amr_ical_list_lang
msgid "Css file to use from plugin directory"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:487
#@ amr_ical_list_lang
msgid "Go to Plugin Editor, select this plugin and scroll to the file"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:489
#@ amr_ical_list_lang
msgid "Edit"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:493
#@ amr_ical_list_lang
msgid " No images (tick for text only)"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:495
#@ amr_ical_list_lang
msgid "Advanced:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:497
#, php-format
#@ amr_ical_list_lang
msgid "Your php version is: %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:499
#, php-format
#@ amr_ical_list_lang
msgid "Your timezone db version is: %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:501
#@ amr_ical_list_lang
msgid "Cannot determine timezonedb version in php &lt; 5.3."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:507
#@ amr_ical_list_lang
msgid "No global timezone - is there a problem here? "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:509
#@ amr_ical_list_lang
msgid "Choose date localisation method:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:512
#@ amr_ical_list_lang
msgid "none"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:514
#@ amr_ical_list_lang
msgid "amr"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:516
#@ amr_ical_list_lang
msgid "wp"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:518
#@ amr_ical_list_lang
msgid "wpgmt"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:537
#@ amr_ical_list_lang
msgid "Saving...."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:545
#@ amr_ical_list_lang
msgid "Invalid List Type"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:552
#@ amr_ical_list_lang
msgid "AmR iCal Events List "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:559
#@ amr_ical_list_lang
msgid "Go to list type:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:569
#@ amr_ical_list_lang
msgid "Save the settings"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:570
#@ amr_ical_list_lang
msgid "Update"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:572
#@ amr_ical_list_lang
msgid "Uninstall the plugin and delete the options from the database."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:573
#@ amr_ical_list_lang
msgid "Uninstall"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:575
#@ amr_ical_list_lang
msgid "Warning: This will reset ALL the options immediately."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:576
#@ amr_ical_list_lang
msgid "Reset"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:598
#@ amr_ical_list_lang
msgid " Define date and time formats:"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:600
#@ amr_ical_list_lang
msgid " These are also used for the grouping headings."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:601
#@ amr_ical_list_lang
msgid "Use the standard PHP format strings: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:602
#@ amr_ical_list_lang
msgid "Php manual - date datetime formats"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:605
#@ amr_ical_list_lang
msgid "date"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:606
#@ amr_ical_list_lang
msgid " (will localise) "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:638
#@ amr_ical_list_lang
msgid "List Type "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-list-admin.php:639
#@ amr_ical_list_lang
msgid "Expand/Contract all"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:6
#@ amr_ical_list_lang
msgid "AmR iCal Options deleted from Database"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:9
#@ amr_ical_list_lang
msgid "AmR iCal Widget Options deleted from Database"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:24
#@ amr_ical_list_lang
msgid "Uninstall AmR iCal Events List Options"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:25
#@ amr_ical_list_lang
msgid "Note this function removes the options from the database.  To completely uninstall, one should continue on to use the standard wordpress functions to deactivate the plugin and delete the files.  It is not necessary to run this separately as the uninstall will also run as part of the wordpress delete plug-in files."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:26
#@ amr_ical_list_lang
msgid "The function is provided here as an aid to someone who has perhaps got their wordpress install in a knot and wishes to temporarily remove the options from the database as part of their debugging or cleanup.  Consider also the use of the RESET."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:34
#@ amr_ical_list_lang
msgid "Note: Navigating to \"Manage AmR ICal Settings\" will RELOAD default options - negating the uninstall."
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:36
#@ amr_ical_list_lang
msgid "Continue to Plugin list to delete files as well"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:45
#@ amr_ical_list_lang
msgid "Cancel"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-ical-uninstall.php:46
#@ amr_ical_list_lang
msgid "Really Uninstall Options?"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:40
#, php-format
#@ amr_ical_list_lang
msgid "Your cache directory %s has been created"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:43
#, php-format
#@ amr_ical_list_lang
msgid "Error creating cache directory %s. Please check permissions"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:91
#@ amr_ical_list_lang
msgid "Invalid URL"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:102
#, php-format
#@ amr_ical_list_lang
msgid "Using File last cached at %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:103
#@ amr_ical_list_lang
msgid "Warning: Events may be out of date. "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:108
#@ amr_ical_list_lang
msgid "No cached ical file for events"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-import-ical.php:686
#, php-format
#@ amr_ical_list_lang
msgid "Error reading cached file: %s"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-rrule.php:510
#@ amr_ical_list_lang
msgid "Unexpected error creating date with string: "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:9
#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:66
#@ amr_ical_list_lang
msgid "Upcoming Events"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:9
#@ amr_ical_list_lang
msgid "events"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:10
#@ amr_ical_list_lang
msgid "AmR Upcoming Events"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:84
#@ amr_ical_list_lang
msgid "See plugin website for more details"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:86
#@ amr_ical_list_lang
msgid "Title"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:89
#@ amr_ical_list_lang
msgid "Calendar page url in this website, for event title links"
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:94
#@ amr_ical_list_lang
msgid "Do an event summary hyperlink with event description as title text "
msgstr ""

#: C:\web\wpbeta\wp\wp-content\plugins\amr-ical-events-list/amr-upcoming-events-widget.php:99
#@ amr_ical_list_lang
msgid "Urls (plus optional shortcode parameters separated by spaces eg: events=10)"
msgstr ""

