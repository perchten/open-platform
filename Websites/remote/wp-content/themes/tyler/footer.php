<?php
/**
 * @package WordPress
 * @subpackage Tyler
 * @since Tyler 1.0
 */
?>
	</div><!-- #contentblock -->


	<div id="footer" role="contentinfo">
		<?php get_sidebar( 'footer' ); ?>

		<div id="copdat">
			<?php printf( __( '%1$s by %2$s' ), 'Tyler', '<a href="http://themestown.com">Themes Town</a>' ); ?> <span class="generator-link"><a href="<?php echo esc_url( __( 'http://wordpress.org/', 'tyler' ) ); ?>" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', 'tyler' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s.', 'tyler' ), 'WordPress' ); ?></a></span>
		</div><!-- #copdat -->
	</div><!-- #footer -->

</div><!-- #container -->
</div><!-- #wrapper -->
</div><!--#contentareawrap-->
</div><!-- #outerwrap -->

<?php wp_footer(); ?>
</body>
</html>