<?php
/**
 * @package WordPress
 * @subpackage Tyler
 * @since Tyler 1.0
 */

get_header(); ?>

	<div id="content-container">
		<div id="content" role="main">

			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'tyler' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'You have arrived at this page because the page you are looking for could not be found.', 'tyler' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #content-container -->
	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>

<?php get_footer(); ?>