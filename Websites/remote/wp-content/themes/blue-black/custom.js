$(function() {
	$('#banner p:first').fadeIn(1000, function() {
	    $('#banner').cycle({ 
		    fx:      'fade', 
		    speed:    1500, 
		    timeout:  10000 
		})
	});
});

$(function() {
		$("#partnerlogos.scroll").simplyScroll({
			autoMode: 'loop'
		});
});

$(function() {
  $('#mailinginfotoggle').click(function() {
    $('#mailinginfo').slideToggle('slow', function() {
      if ( $('#mailinginfotoggle').text().indexOf('More')==0 ) {  
        $("#mailinginfotoggle").text('Less info...');
      } else {
        $("#mailinginfotoggle").text('More info...');
      }
    });
  })
});

$(function() { //on DOM ready
    $('.amrcol .description').each( function() {
                    $(this).jTruncate({  
                            length: 100,  
                            minTrail: 0,  
                            moreAni: "fast",  
                            lessAni: 2000  
                    });  
            });
    $('.description').each(function(){
            $(this).find("a").each(function(){
                    $(this).css('display','inline');
                    $(this).css('padding','0');
                    $(this).css('margin','0');
                    $(this).css('float','none');
                    $(this).css('width','auto');				
            });
            $(this).find("a.truncate_more_link").each(function () {
                    $(this).css('display','block');
                    $(this).css('width','240px');				
                    $(this).css('text-align','right');
            });			
            $(this).find("a:contains('facebook')").each(function () {			
                    $(this).text('View on Facebook');
                    $(this).css('display','block');
                    $(this).css('text-align','center');
                    $(this).css('width','240px');
                    $(this).css('margin','0 auto');
            });

    });

    $('a').each( function() {        
        if ( !$(this).parents('.menu').length ) {
           var h = $(this).attr('href');
           if ( h && h.substring(0,1)!="#" && h.substring(0,4)=="http") {
              var idex = h.indexOf('openplatform.org.uk');
              if ( idex == -1 || idex > 11 ) {
                  $(this).attr('target', '_blank');
              }
          }
        }
    });
    $('li.iframe a').addClass("fancybox iframe");

//    $('.widget:first a').css('font-size','1.3em');
//    $('.widget:first a').css('padding-bottom','3px');
//    $('.widget:first a').css('margin-top','3px');
//    $('.widget:first').css('padding-bottom','12px');
});

$(function() { //on DOM ready
        
        function truncateEvents(parentClass, truncateLimit) {
            $(parentClass+' .fb-event-description').each( function() {
                if ( truncateLimit>0 ) {
                    $(this).jTruncate({  
                        length: truncateLimit,  
                        minTrail: 0,  
                        moreAni: "fast",  
                        lessAni: 2000  
                    });  
                } else {
                    
                    var moreLessDiv = $(document.createElement("div"));
                    moreLessDiv.addClass("more-less-links");
                    var moreA = $(document.createElement("a"));
                    moreA.append("more");
                    var lessA = $(document.createElement("a"));
                    lessA.append("less");
                    moreLessDiv.append(moreA);
                    moreLessDiv.append(lessA);
                    $(this).after(moreLessDiv);
                    
                    
                    var description = $(this);
                    lessA.hide();
                    description.hide();
                    
                    
                    moreA.click(function(){
                        description.slideToggle();
                        moreA.hide();
                        lessA.show();
                    });
                    lessA.click(function(){
                        description.slideToggle();
                        lessA.hide();
                        moreA.show();                        
                    })
                    
                }                
            });
            $(parentClass+' .fb-event-description').each(function(){
//                $(this).find("a").each(function(){
//                    $(this).css('display','inline');
//                    $(this).css('padding','0');
//                    $(this).css('margin','0');
//                    $(this).css('float','none');
//                    $(this).css('width','auto');				
//                });
//                $(this).find("a.truncate_more_link").each(function () {
//                    $(this).css('display','block');
//                    $(this).css('width','240px');				
//                    $(this).css('text-align','right');
//                });			
                $(this).find("a:contains('facebook')").each(function () {			
                    $(this).text('View on Facebook');
                    $(this).css('display','block');
                    $(this).css('text-align','center');
                    $(this).css('width','240px');
                    $(this).css('margin','0 auto');
                });
            });
        }
    
        truncateEvents(".widget .fb-event.OPEN",141);
        truncateEvents(".entry .fb-event.OPEN",640);
        truncateEvents(".fb-event.SECRET",0);
        
        
});