<div id="banner">
    <?php
        $dir = ABSPATH.'wp-content/themes/blue-black/opimages/bannerimages';
        $handle = opendir($dir);	
        $files = array();			
        while (false !== ($file = readdir($handle))) {
            if ( strpos(strtolower($file),'jpg')!==false) { 
				$files[] = $file;
			}
        }
        closedir($handle);
        shuffle($files);
      
        foreach ( $files as $file ) {
		$file = get_bloginfo('template_directory').'/opimages/bannerimages/'.$file;
            echo '<p class="bannerimg" style="background: transparent url('.$file.') top center no-repeat" ></p>';
        }
       
    ?>
</div>   