<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/vnd.microsoft.icon"/>
<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-ico"/>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <!--[if IE 6]>
	<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/hack.css" type="text/css" />
	<![endif]-->
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/simplyscroll/jquery.simplyscroll-1.0.4.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/partnerlogos.css" />
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/simplyscroll/jquery.simplyscroll-1.0.4.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/eventslider.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bannerrotate.css" />
<script type="text/javascript" src="http://malsup.github.com/chili-1.7.pack.js"></script>
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.lite.1.0.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/custom.js"></script>
<?php wp_head(); ?>
</head>
<body>
<!--wrapper-->
<div id="wrapper">
<!--header-->
	<div id="header">    	
        <!--blog-title-->
        <p id="blog-title"><?php bloginfo('description'); ?></p>
		<p id="blog-logo"><a href="<?php echo get_option('home'); ?>" title="<?php bloginfo('description'); ?>"><img style="border:none;text-decoration:none;" src="<?php bloginfo('template_directory'); ?>/opimages/OP-logo_text.png" /></a></p>
        
        <!--search-->  

    	<?php //include (TEMPLATEPATH . '/searchform.php'); ?>
        <!--page-navigation-->
        <?php wp_nav_menu(); ?>
        <?php include('bannerrotate.php'); ?>     

    </div><!--header-end-->
    
<?php include('partnerlogos.php'); ?>
        
    