<?php
/*
Template Name: Events Page
*/
?>
<?php get_header(); ?>

	<!--content-->
	<div id="content">
            <?php include('system-warning.php'); ?>
		<!--left-col-->   
		<div id="left-col">


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
        
            <div class="entry">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            
            <?php the_content(__('<span class="more">READ MORE</span>'));?>
            
            <?php
                $appId = "183075745110471";
                $appSecret = "3aa601da6a854f5d0f5109305c410008";
                $pageId = "73865530585";
                $maxEvents = 99;
                $futureEvents = true;
                $timeOffset = 8;
                
                $fbevents = new Facebook_Events_Widget();
                $fqlResult = $fbevents->query_fb_page_events($appId, $appSecret, $pageId, $maxEvents, $futureEvents);
                
                
                
                echo '<div class="fb-events-container">';
		//looping through retrieved data
		if (!empty($fqlResult)) {
                    $first = true;
                    foreach ($fqlResult as $keys => $values) {
                            $fbevents->create_event_div_block($values, $timeOffset, $first);
                            $first = false;
                    }
		}
		echo '</div>';

            ?>
            
            
            </div>
		</div>
 	       <div class="post-bg-down"></div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
    
</div><!--left-col-end--> 
<?php get_sidebar(); ?>
</div><!--content-end-->

</div><!--wrapper-end-->
<?php get_footer(); ?>