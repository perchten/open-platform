<?php 
function get_partnerlogos($scrolling=true, $randomise=true) { 

    $wpPartners = get_posts(array('post_type'=>'partner','numberposts'=>'-1'));
    if ( $randomise ) {
        shuffle($wpPartners);	
    }    
    $markup = "";        
    foreach ($wpPartners as $wpPartner) {
        $partner = get_post_complete($wpPartner->ID);
        
        $img = get_post($partner->partnerlogo);        
        $html = '<p class="img"><img width=75 src="'.$img->guid.'" /></p class="text"><p>'.$partner->post_title.'</p>';  
        if ( $partner->partnerurl ) {
            $html = '<a class="logo" title="'.$partner->post_excerpt.'" href="'.$partner->partnerurl.'">'.$html.'</a>';
        } else {
            $html = '<div class="logo" title="'.$partner->post_excerpt.'">'.$html.'</div>';
        }
        if ( !$scrolling ) {
            $html = '<li>'.$html.'</li>';
        }
        $markup .= $html;
    }
    if ( !$scrolling ) {
        $markup = "<ul>".$markup."</ul>";
    }
    $scrollclass = $scrolling? "scroll" : "noscroll";    
    $markup = '<div id="partnerlogos" class="'.$scrollclass.'">'.$markup.'</div>';
    return $markup;
}

function partnerlogos() {
	echo get_partnerlogos();	
}

?>