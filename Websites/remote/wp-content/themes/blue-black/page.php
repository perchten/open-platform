<?php get_header(); ?>

	<!--content-->
	<div id="content">
            <?php include('system-warning.php'); ?>
		<!--left-col-->   
		<div id="left-col">


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
        
            <div class="entry">
            <?php if ( !is_front_page() ) { ?><h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2><?php } ?>
            
            <?php the_content(__('<span class="more">READ MORE</span>'));?>
            
            </div>
		</div>
 	       <div class="post-bg-down"></div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

	<?php if (is_front_page()) { ?>
	
    
    <div class="frontpage_block">
		        
        <div id="signup">
            <table border=0 style="padding: 5px;" cellspacing=0>
              <tr><td><img src="<?php bloginfo('template_directory'); ?>/opimages/groups_logo_sm.png" height=30 width=140 alt="Google Groups"></td></tr>
              <form action="http://groups.google.com/group/openplatformbristol/boxsubscribe">
              <tr><td style="padding-left: 5px;">
              		Email: <input type=text name=email>
                    <input type=submit name="sub" value="Subscribe">
              </td></tr>
            	</form>
              <tr><td align=right><a href="http://groups.google.com/group/openplatformbristol">Visit the google group page</a></td></tr>
            </table>
            <a id="mailinginfotoggle">More info...</a> 
            <div id="mailinginfo">
              <p>This mailing list is only used to keep you updated about upcoming events.</p>
              <p>You'll receive at most one mailout per week from the committee plus automated reminders from our calendar so that you don't accidentally find yourself missing a session you wanted to come along to.</p>
              <p>Members cannot post to this list, but you can start discussions if you join us on facebook.</p>
            </div>
        </div>

		<div id="video">
            <p style="text-align:center;margin: 0 auto"><object width="280" height="227"><param name="movie" value="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="280" height="227"></embed></object></p>
        </div>

	</div>            
    
<!--        <div id="video">
        	<p>    
            <object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>
            </p>
        </div> -->
<!--	    <div id="facebook">
	    	<iframe src="http://www.facebook.com/plugins/likebox.php?id=73865530585&amp;width=598&amp;colorscheme=light&amp;connections=10&amp;header=true&amp;height=233" scrolling="no" frameborder="0" style="margin-top:2em;border:none; width:600px; height:235px; overflow:auto " allowTransparency="true"></iframe>
	    </div>-->
	
		
		<div id="partner_block">
			<?php partnerlogos(); ?>
                    <p style="text-align:center"><a href="<?php echo get_bloginfo('url')?>/friends">See all Friends &amp; Partners</a></p>
		</div>
    <?php } ?>

    
</div><!--left-col-end--> 
<?php get_sidebar(); ?>
</div><!--content-end-->

</div><!--wrapper-end-->
<?php get_footer(); ?>