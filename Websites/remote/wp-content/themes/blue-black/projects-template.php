<?php
/*
Template Name: Projects Page
*/
?>
<?php get_header(); ?>

	<!--content-->
	<div id="content">
            <?php include('system-warning.php'); ?>
		<!--left-col-->   
		<div id="left-col">


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
        
            <div class="entry">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            
            <?php 
                    $projectTypeMeta = get_custom_field_meta("project_type",'options');
                    $projectTypeOptions = $projectTypeMeta['options'];
                    $projectTypeValues = $projectTypeMeta['values'];
                    $projectTypes = array_combine($projectTypeOptions, $projectTypeValues);
                    
                    $i = 0;
                    echo '<ul class="in-post-menu">';
                    foreach ( $projectTypes as $projectTypeLowercasePlural => $projectTypeUppercaseSingular) {
                        if ( $i>0 ) {
                            echo " | ";
                        }
                        echo '<li><a href="#'.$projectTypeLowercasePlural.'">'.ucfirst($projectTypeLowercasePlural).'</a></li>';
                        $i++;                                                                        
                    }                    
                    echo "</ul>";
            ?>
            
            <?php the_content(__('<span class="more">READ MORE</span>'));?>
            
            <?php
            
            
                // Grab the project type descriptions
                // Simple, just get all the descriptions and assign them to an array based on the project type they're associated with,
                // Obviously, in cases of multiple assignations to the same project type the last one is the only one retained after this loop
                $projectTypeDescriptions = get_posts(array("post_type"=>"project_type_descrip", 'numberposts'=>-1));
                foreach ( $projectTypeDescriptions as $projectTypeDescription ){
                    $fullProjectTypeDescription = get_post_complete($projectTypeDescription->ID);
                    $projectTypeDescriptions[$fullProjectTypeDescription->project_type] = $fullProjectTypeDescription->post_content;
                }

                
                $projectsByType = array();
                // Loop through actual projects and assign them in an array based on their project types
                $projects = get_posts(array("post_type"=>"project","numberposts"=>-1,'order'=>"DESC", 'orderby'=>'menu_order'));
                foreach ( $projects as $project) {
                    $fullProject = get_post_complete($project->ID);
                    $projectsByType[$fullProject->project_type][] = $fullProject;
                }
                
            
                foreach ( $projectTypes as $projectTypeLowercasePlural => $projectTypeUppercaseSingular ) {
                    
                    echo '<h3 id="'.$projectTypeLowercasePlural.'">'.ucfirst($projectTypeLowercasePlural).'</h3>';
                    
                    if ( array_key_exists($projectTypeUppercaseSingular, $projectTypeDescriptions) ) {
                        echo $projectTypeDescriptions[$projectTypeUppercaseSingular];
                    }
                    
                    if (array_key_exists($projectTypeUppercaseSingular, $projectsByType)) {
                        echo '<ul class="projectlist">';
                        foreach($projectsByType[$projectTypeUppercaseSingular] as $project ) {
                            echo "<li>";
                            
                            $image = wp_get_attachment_image( $project->project_image, "project_thumbnail");
                            $title = $project->post_title;
                            if ( $project->project_url!=null ) {
                                $link = '<a href="'.$project->project_url.'">';
                                $image = $link.$image."</a>";
                                $title = $link.$title."</a>"; 
                            }
                            $title = '<p class="title">'.$title.'</p>';
                            
                            echo $image;
                            echo $title;
                            echo $project->post_content;
                            echo "</li>";
                            echo $html;
                        }
                        echo '</ul><p style="clear:both"></p>';
                    } else {
                        echo '<p class="noresults">No '.ucfirst($projectTypeLowercasePlural).' exist';
                    }
                }
            
            ?>
            
            
            </div>
		</div>
 	       <div class="post-bg-down"></div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
    
</div><!--left-col-end--> 
<?php get_sidebar(); ?>
</div><!--content-end-->

</div><!--wrapper-end-->
<?php get_footer(); ?>