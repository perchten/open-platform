<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/vnd.microsoft.icon"/>
<link rel="icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" type="image/x-ico"/>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <!--[if IE 6]>
	<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/hack.css" type="text/css" />
	<![endif]-->
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/simplyscroll/jquery.simplyscroll-1.0.4.css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/partnerlogos.css" />
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/simplyscroll/jquery.simplyscroll-1.0.4.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/eventslider.js"></script>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bannerrotate.css" />
<script type="text/javascript" src="http://malsup.github.com/chili-1.7.pack.js"></script>
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.lite.1.0.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#banner p:first').fadeIn(1000, function() {
	    $('#banner').cycle({ 
		    fx:      'fade', 
		    speed:    1500, 
		    timeout:  10000 
		})
	});
});
</script>
<script type="text/javascript">
(function($) {
	$(function() { //on DOM ready
		$("#partnerlogos").simplyScroll({
			autoMode: 'loop'
		});
	});
})(jQuery);
</script>
<script type="text/javascript">
$(function() {
  $('#mailinginfotoggle').click(function() {
    $('#mailinginfo').slideToggle('slow', function() {
      if ( $('#mailinginfotoggle').text().indexOf('More')==0 ) {  
        $("#mailinginfotoggle").text('Less info...');
      } else {
        $("#mailinginfotoggle").text('More info...');
      }
    });
  })
});
</script>
<script type="text/javascript">
(function($) {
	$(function() { //on DOM ready
	$('.amrcol div.description').each( function() {
			$(this).jTruncate({  
				length: 100,  
				minTrail: 0,  
				moreAni: "fast",  
				lessAni: 2000  
			});  
		});
	});
})(jQuery);
(function($) {
	$(function() { //on DOM ready
		$('.amrcol div.description .truncate_more a').text('View on Facebook');
		$('.amrcol div.description a').css('text-align','right');
		$('.amrcol div.description a').css('width','240px');
		  
	});
})(jQuery);</script>
<?php wp_head(); ?>
</head>
<body>
<!--wrapper-->
<div id="wrapper">
<!--header-->
	<div id="header">    	
        <!--blog-title-->
        <p id="blog-title"><?php bloginfo('description'); ?></p>
		<p id="blog-logo"><a href="<?php echo get_option('home'); ?>" title="<?php bloginfo('description'); ?>"><img style="border:none;text-decoration:none;" src="<?php bloginfo('template_directory'); ?>/opimages/OP-logo_text.png" /></a></p>
        
        <!--search-->  

    	<?php //include (TEMPLATEPATH . '/searchform.php'); ?>
        <!--page-navigation-->
        <div id="menu">
        	<ul>
				<!--<li class="page_item"><a href="<?php echo get_option('home'); ?>/">HOME</a></li>-->
				<?php wp_list_pages('sort_order=desc&sort_column=menu_order&title_li=&depth=1&exclude=6,23,45,131,139'); ?>
            </ul>
        </div><!--page-navigation-->
        
        <?php include('bannerrotate.php'); ?>     

        
    </div><!--header-end-->

<?php include('partnerlogos.php'); ?>
