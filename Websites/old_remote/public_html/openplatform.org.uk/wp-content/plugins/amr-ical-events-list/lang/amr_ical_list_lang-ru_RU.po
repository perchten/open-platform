msgid ""
msgstr ""
"Project-Id-Version: AMR\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-07-12 16:06+0100\n"
"PO-Revision-Date: 2010-01-01 18:37+0200\n"
"Last-Translator: Fat Cow <zhr@tut.by>\n"
"Language-Team: ghost <iamm@tut.by>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Russian\n"
"X-Poedit-Country: RUSSIAN FEDERATION\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:309
msgid " Define date and time formats:"
msgstr "определить формат даты и времени"

#@ amr-ical-events-list
#: amr-ical-events-list.php:112
msgid "Add event to google"
msgstr "добавить событие в google"

#@ amr-ical-events-list
#: amr-ical-events-list.php:110
msgid "Add event to your Google Calendar"
msgstr "добавить событие к вашему календарю google"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:610
msgid "AmR ICal Global Options"
msgstr "AmR ICal глобальные настройки"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:92
msgid "AmR iCal Event List Configuration"
msgstr "AmR iCal  конфигурация списка событий"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:91
msgid "AmR iCal Events List"
msgstr "AmR iCal список событий"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:592
msgid "AmR iCal Events List "
msgstr "AmR iCal спиоск событий"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:6
msgid "AmR iCal Options deleted from Database"
msgstr "Опции AmR iCal удалены с базы данных"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:9
msgid "AmR iCal Widget Options deleted from Database"
msgstr "опции виджетов AmR iCal удалены избазы данных"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:420
msgid "Calendar properties"
msgstr "свойства календаря"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:48
msgid "Cancel"
msgstr "удалить"

#@ amr-ical-events-list
#: amr-ical-config.php:325
msgid "Class"
msgstr "класс"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:469
msgid "Column Headings"
msgstr "колонна заголовка"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:37
msgid "Continue to Plugin list to delete files as well"
msgstr "продолжать список плагинов для удаления файлов"

#@ amr-ical-events-list
#: amr-ical-config.php:324
msgid "Date"
msgstr "дата"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:404
msgid "Define grouping:"
msgstr "определить группирование:"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:366
msgid "Define maximums:"
msgstr "определить мксимальный:"

#@ amr-ical-events-list
#: amr-ical-config.php:312
#: amr-ical-config.php:350
msgid "Description"
msgstr "описание"

#@ amr-ical-events-list
#: amr-ical-events-list.php:584
msgid "Event Link"
msgstr "ссылка на событие"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:667
msgid "List Type "
msgstr "тип списка"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:68
msgid "List Type from plugin settings"
msgstr "ти списка с настроек плагина"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:26
msgid "Note this function removes the options from the database.  To completely uninstall, one should continue on to use the standard wordpress functions to deactivate the plugin and delete the files.  It is not necessary to run this separately as the uninstall will also run as part of the wordpress delete plug-in files."
msgstr "Эта функция удалит все настройки из базы данных.  To completely uninstall, one should continue on to use the standard wordpress functions to deactivate the plugin and delete the files.  It is not necessary to run this separately as the uninstall will also run as part of the wordpress delete plug-in files."

#@ amr-ical-events-list
#: amr-ical-uninstall.php:35
msgid "Note: Navigating to \"Manage AmR ICal Settings\" will RELOAD default options - negating the uninstall."
msgstr "Памятка: Навигация по \"Manage AmR ICal Settings\" will RELOAD default options - negating the uninstall."

#@ amr-ical-events-list
#: amr-ical-list-admin.php:71
msgid "Number of Events"
msgstr "количество событий"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:611
msgid "Number of Ical Lists:"
msgstr "Количество Ical Lists:"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:294
msgid "Options  <strong>Updated</strong>. "
msgstr "настройки <strong> обновить <strong>."

#@ amr-ical-events-list
#: amr-ical-uninstall.php:49
msgid "Really Uninstall Options?"
msgstr "модифицировать опции?"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:655
msgid "Reset"
msgstr "воостановить"

#@ amr-ical-events-list
#: amr-ical-config.php:326
msgid "Room"
msgstr "комната"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:384
msgid "Select components to show:"
msgstr "выбрать компанентыдля показа:"

#@ amr-ical-events-list
#: amr-ical-config.php:292
msgid "Show in Google map"
msgstr "показать в карте google"

#@ amr-ical-events-list
#: amr-ical-config.php:290
msgid "Show location in Google Maps"
msgstr "показать места в картах google"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:441
msgid "Specify component contents:"
msgstr "определить еомпаненты содержания:"

#: amr-ical-uninstall.php:27
msgid "The function is provided here as an aid to someone who has perhaps got their wordpress install in a knot and wishes to temporarily remove the options from the database as part of their debugging or cleanup.  Consider also the use of the RESET."
msgstr "Функция, представленная здесь в качестве помощи для тех, кто имеет, возможно, получили их установке WordPress в узел и желает временно удалить параметры из базы данных в качестве части их отладки или очистке. Рассмотрим также использование RESET."

#@ amr-ical-events-list
#: amr-ical-list-admin.php:66
msgid "Title"
msgstr "название"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:656
msgid "Uninstall"
msgstr "модифицировать"

#@ amr-ical-events-list
#: amr-ical-uninstall.php:25
msgid "Uninstall AmR iCal Events List Options"
msgstr "модифицировать AmR iCal опции списка событий "

#@ amr-ical-events-list
#: amr-ical-config.php:595
msgid "Upcoming Events"
msgstr "предстоящие события"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:654
msgid "Update"
msgstr "обновить"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:78
msgid "Urls"
msgstr "Urls"

#@ amr-ical-events-list
#: amr-ical-config.php:311
msgid "Venue"
msgstr "место проведения"

#@ amr-ical-events-list
#: amr-ical-events-list.php:929
#, php-format
msgid "Week  %u"
msgstr "наделя %u"

#@ amr-ical-events-list
#: amr-ical-config.php:193
msgid "What"
msgstr "что"

#@ amr-ical-events-list
#: amr-ical-config.php:192
msgid "When"
msgstr "когда"

#@ amr-ical-events-list
#: amr-ical-config.php:194
msgid "Where"
msgstr "где"

#@ amr-ical-events-list
#: amr-ical-events-list.php:232
msgid "calendar"
msgstr "календарь"

#@ amr-ical-events-list
#: amr-ical-list-admin.php:321
msgid "strftime"
msgstr "Strftime"

#@ amr-ical-events-list
#: amr-ical-config.php:171
msgid "Year"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:172
msgid "Quarter"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:173
msgid "Astronomical Season"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:174
msgid "Traditional Season"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:175
msgid "Western Zodiac"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:176
msgid "Month"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:177
msgid "Week"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:178
msgid "Day"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:351
msgid "Timing"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:626
msgid "Events plugin by anmari"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:644
msgid "No events found within start and end date"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:654
msgid "Resetting options..."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:655
msgid "Options Deleted..."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:656
msgid "Error deleting option..."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:657
msgid "Options updated with defaults..."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-config.php:666
msgid " Converting option key to lowercase"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:47
msgid "The <a href=\"http://au.php.net/manual/en/class.datetime.php\"> DateTime Class </a> must be enabled on your system for this plugin to work. They may need to be enabled at compile time.  The class should exist by default in PHP version 5.2."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:92
msgid "Add this calendar to your google calendar"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:95
msgid "Add to your Google Calendar"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:193
#, php-format
msgid "Refresh calendars - last refresh was at %s"
msgstr ""

#: amr-ical-events-list.php:207
msgid "Total events: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:212
#: amr-ical-events-list.php:222
#, php-format
msgid "Subscribe to %s Calendar"
msgstr ""

#: amr-ical-events-list.php:418
#, php-format
msgid "%u year"
msgid_plural "%u years"
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:423
#, php-format
msgid "%u month "
msgid_plural "%u months "
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:428
#, php-format
msgid "%u week "
msgid_plural "%u weeks"
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:434
#, php-format
msgid "%u day"
msgid_plural "%u days"
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:440
#, php-format
msgid "%u hour"
msgid_plural "%u hours"
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:445
#, php-format
msgid "%u minute"
msgid_plural "%u minutes"
msgstr[0] ""
msgstr[1] ""

#: amr-ical-events-list.php:450
#, php-format
msgid "%u second"
msgid_plural "%u seconds"
msgstr[0] ""
msgstr[1] ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:465
#, php-format
msgid "Timezone: %s, Click for %s"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:514
msgid "No event description available"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1284
#, php-format
msgid "Unable to load or cache ical calendar %s"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1301
msgid "Start date: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1302
msgid "End date: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1427
msgid "Invalid Start date"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1457
#, php-format
msgid "Invalid Ical URL passed in query string %s"
msgstr ""

#: amr-ical-events-list.php:1510
msgid "Settings"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:44
msgid "Jump Back in days:"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:47
msgid "Back"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:51
msgid "Show less days and events"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:53
msgid "Show Less"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:56
msgid "Show more days and events"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:57
msgid "Show More"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:61
msgid "Start from day:"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-plusHIDE.php:62
msgid "Forward"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:37
msgid "Invalid URL: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:51
msgid "Invalid List Type entered:"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:73
msgid "Check \"days\" in list type settings too"
msgstr ""

#: amr-ical-list-admin.php:247
msgid "Error in form - calprop array not found"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:311
msgid " These are also used for the grouping headings."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:312
msgid "Use the standard PHP format strings: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:313
msgid "Php manual - date datetime formats"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:316
msgid "date"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:317
msgid " (will localise), "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:318
msgid "Php manual - Strftime datetime formats"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:533
msgid "Significant effort goes into these plugins to ensure that they <strong>work straightaway</strong> with minimal effort, are easy to use but <strong>very configurable</strong>, that they are <strong>well tested</strong>,that they produce <strong>valid html and css</strong> both at the front and admin area. If you wish to remove the credit link or using the plugin commercially, then please donate."
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:535
msgid "Donate"
msgstr ""

#: amr-ical-list-admin.php:536
msgid "Support at Wordpress"
msgstr ""

#: amr-ical-list-admin.php:538
msgid "Plugin website"
msgstr ""

#: amr-ical-list-admin.php:559
msgid "Unable to create Custom css file for you to edit if you wish - not essential."
msgstr ""

#: amr-ical-list-admin.php:563
#, php-format
msgid "Copied %s1 to %s2 to allow custom css"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:599
msgid "Timezone for date and time calculations is "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:618
msgid "Donation made"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:622
msgid " Do not generate css"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:624
msgid "Css file to generate from plugin directory"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:639
msgid "Go to Plugin Editor, select this plugin and scroll to the file"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:641
msgid "Edit"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:646
msgid "Message if no events found: "
msgstr ""

#@ amr-ical-events-list
#: amr-import-ical.php:42
#, php-format
msgid "Your cache directory %s has been created"
msgstr ""

#@ amr-ical-events-list
#: amr-import-ical.php:45
#, php-format
msgid "Error creating cache directory %s. Please check permissions"
msgstr ""

#@ amr-ical-events-list
#: amr-import-ical.php:100
#, php-format
msgid "Calendar file not found: %s"
msgstr ""

#@ amr-ical-events-list
#: amr-import-ical.php:467
#, php-format
msgid "Error reading cached file: %s"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:189
msgid "Last Refresh time unexpectedly not available"
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1299
msgid "Days to show: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-events-list.php:1300
msgid "Events to show: "
msgstr ""

#@ amr-ical-events-list
#: amr-ical-list-admin.php:75
msgid "Calendar page url in this website, for event title links"
msgstr ""

