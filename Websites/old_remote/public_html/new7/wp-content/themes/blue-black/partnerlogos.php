<?php 
function get_partnerlogos() { 

	$entries = array();
	$entries[] = '<a class="logo" href="http://www.schumacherinstitute.org.uk/"><p class="img"><img width=100 src="'.get_bloginfo('template_directory').'/opimages/siss.png" /></p class="text"><p>Schumacher Institute for Sustainable Systems</p></a>';
	$entries[] = '<a class="logo" href="http://www.transitionmentoring.com/"><p class="img"><img width=100 src="http://www.transitionmentoring.com/images/logo.png" /></p><p class="text">Transition Mentoring</p></a>';
	$entries[] = '<a class="logo" href="http://bristolhub.org/bust/"><p class="img"><img width=100 src="'.get_bloginfo('template_directory').'/opimages/bust.png" /></p><p class="text">Bristol University Sustainability Team</p></a>';
	$entries[] = '<a class="logo" href="http://www.gaiacoach.co.uk/"><p class="img"><img width=100 src="'.get_bloginfo('template_directory').'/opimages/gaiacoach.png" /></p><p class="text">Gaia Coach Institute</p></a>';
	$entries[] = '<a class="logo" href="http://www.theconvergingworld.org//"><p class="img"><img width=100 src="'.get_bloginfo('template_directory').'/opimages/tcw.png" /></p><p class="text">The Converging World</p></a>';
	$entries[] = '<a class="logo" href="http://www.350.org/"><p class="img"><img width=100 src="http://www.350.org/sites/all/themes/threefifty/logo.gif" /></p><p class="text">350.org</p></a>';
	$entries[] = '<div class="logo"><p class="img"><img width=100 src="'.get_bloginfo('template_directory').'/opimages/durable_media.png" /></p><p class="text">Durable Media</p></div>';
	 

	$markup = '<div id="partnerlogos">';
	shuffle($entries);						
	foreach ( $entries as $entry ) {
		$markup .= $entry;	
	}					
	$markup .= '</div>';
	return $markup;
}

function partnerlogos() {
	echo get_partnerlogos();	
}

?>