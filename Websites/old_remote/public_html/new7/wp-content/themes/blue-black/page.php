<?php get_header(); ?>

	<!--content-->
	<div id="content">
    
		<!--left-col-->   
		<div id="left-col">


		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
        
        <div class="entry">
		<?php if ( !is_front_page() ) { ?><h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2><?php } ?>
		
		<?php the_content(__('<span class="more">READ MORE</span>'));?>
        
		</div>
		</div>
        <div class="post-bg-down"></div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

	<?php if (is_front_page()) { ?>
	
<!--        <div id="video">
        	<p>    
            <object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/iraGmFHqfb4?hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>
            </p>
        </div> -->
	    <div id="facebook">
	    	<iframe src="http://www.facebook.com/plugins/likebox.php?id=73865530585&amp;width=598&amp;colorscheme=light&amp;connections=10&amp;header=true&amp;height=233" scrolling="no" frameborder="0" style="margin-top:2em;border:none; width:600px; height:235px; overflow:auto " allowTransparency="true"></iframe>
	    </div>
	
		
		<div id="partner_block">
			<?php partnerlogos(); ?>
		</div>
    <?php } ?>

    
</div><!--left-col-end--> 
<?php get_sidebar(); ?>
</div><!--content-end-->

</div><!--wrapper-end-->
<?php get_footer(); ?>