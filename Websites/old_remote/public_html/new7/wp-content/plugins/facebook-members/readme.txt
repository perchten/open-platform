=== Facebook Members ===
Contributors: arpitshah
Tags: facebook, FB members, facebook plugin, like box, likebox, facebook like box, facebook integration
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=10641755
Requires at least: 2.9
Tested up to: 3.1
Stable tag: 2.9.0

Facebook Members is a WordPres Social Plugin that enables Facebook Page owners to attract and gain Likes from their own website.

== Description ==
Facebook Members is a WordPres Social Plugin that enables Facebook Page owners to attract and gain Likes from their own website. It uses Facebook Like Box. 

See how many users already like this page, and which of their friends like it too. Read recent posts from the page Like the page with one click, without needing to visit the page. Get more visitors and more traffic to your site by getting more Facebook Fans.

**[Screenshots - Admin Panel and Preview](http://wordpress.org/extend/plugins/facebook-members/screenshots/)**

**Features**

* Uses Official Facebook APIs.
* Facebook Like Box Plugin Integration.
* Show Like Box on any `Page or Post`.
* Show Like Box in `Sidebar/Widget Area`.
* Option to specify Width, Height, Number of Connections, Stream, Heading for both Sidebar and Post/Page widget.

Tags: facebook, FB members, facebook plugin, like box, likebox, facebook like box, facebook integration

**List of my WordPress Plugins**

* <a href="http://crunchmeme.com/plugins/facebook-members/" target="_blank">Facebook Members</a>
* <a href="http://crunchmeme.com/plugins/wp-google-buzz/" target="_blank">WP Google-buzz</a>
* <a href="http://crunchmeme.com/plugins/all-in-one-webmaster/" target="_blank">All in one Webmaster</a>
* <a href="http://crunchmeme.com/plugins/wp-archive-sitemap-generator/" target="_blank">WP Archive-Sitemap Generator</a>
* <a href="http://crunchmeme.com/plugins/twitter-goodies/" target="_blank">Twitter Goodies</a>
* <a href="http://crunchmeme.com/plugins/foursquare-integration/" target="_blank">FourSquare Integration</a>

== Installation ==
1. Unpack the `download-package`.
2. Upload the file to the `/wp-content/plugins/` directory.
3. Activate the plugin through the `Plugins` menu in WordPress.
4. Configure the options under Admin Panel `Settings -> Facebook Members`.
5. Done and Ready.
6. Please see FAQ for usage.

== Frequently Asked Questions ==

= How can I get Facebook Member Widget to my blog's Page or Post? =
* Please add `<!--facebook-members-->` to any page/post under HTML section. 

= How can I get Facebook Members to blog's sidebar? =
* Go to `Appearance -> Widgets -> Facebook Members` and add it into Sidebar.

= What should I put in page name section? =
* You need to provide whatever is after `facebook.com/`........ (URL).

= Limited customization Option =
* Please visit: http://crunchmeme.com/facebook-like-box-remove-border/

= Do you have any question? =
* Please report your questions or bugs at Plugin Homepage site. 

= Live Examples =
* Go to Plugin Homepage here http://crunchmeme.com/plugins/facebook-members.

== Screenshots ==
1. Admin Panel Options for Post/Page Widget
2. Admin Panel Options for Sidebar Widget
3. Sample Example

== Changelog ==

= 2.9.0 =
* <a href="http://crunchmeme.com/plugins/facebook-members/" target="_blank">Changelog</a>

= Previous Versions =
* div id="likebox-frame" added to Widget. 
* You can now remove border from Facebook Member's Widget. See this for update: http://crunchmeme.com/facebook-like-box-remove-border/
* Other minor update 
* Added [CrunchMeme Fan Page](http://www.facebook.com/pages/CrunchMeme/163787073654972) Like button to admin panel.
* Minor update
* CSS customization option will be in next release
* Show Facebook Members on Post/Page
* More options will be in next release
* Initial but stable release :) 